from setuptools import setup

setup(
    name='Spectacl',
    version='1.0',
    packages=[''],
    url='https://sfb876.tu-dortmund.de/spectacl/index.html',
    license='',
    author='Philipp-Jan Honysz',
    author_email='philipp.honysz@udo.edu',
    description=''
)
